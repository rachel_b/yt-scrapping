from scrapy.spider import BaseSpider
from scrapy.selector import  Selector
from scrapy.http import FormRequest, Request
import  logging
from scrapy import Field
from scrapy import Item


class Product(Item):
    Test =  Field()


class LoginSpider(BaseSpider):
    name = 'super'
    start_urls = ['https://accounts.google.com/ServiceLogin?service=mail&continue=https://mail.google.com/mail/&hl=fr#identifier']

    def parse(self, response):
        return [FormRequest.from_response(response,
                    formdata={'Email': 'useryt00001@gmail.com', 'Passwd': 'Rnii9bEdhcakND'},

                    callback=self.after_login)]

    def after_login(self, response):
      if "authentication failed" in response.body:
        self.log("Login failed", level=logging.ERROR)
        return Request(url="https://mail.google.com/mail/u/0/#inbox",
                   callback=self.parse_tastypage)
    # We've successfully authenticated, let's have some fun!
    print("Login Successful!!")


    def parse_tastypage(self, response):
      item = Product()
      item ["Test"] = response.xpath("//h1/text()").extract()
      yield item
